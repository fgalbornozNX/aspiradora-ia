﻿using UnityEngine;

public class scr_floor : MonoBehaviour
{
    private bool sucio = false; // true/false;

    public void HayBasura()
    {
        if (!sucio)
            sucio = true;
    }

    public void Limpiar()
    {
        if(sucio)
            sucio = false;
    }

    public bool EstaLimpio()
    {
        return !sucio;
    }
}
