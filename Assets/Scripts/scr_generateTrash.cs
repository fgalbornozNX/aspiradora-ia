﻿using UnityEngine;
using UnityEngine.UI;

public class scr_generateTrash : MonoBehaviour
{
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private scr_floor floorA;
    [SerializeField] private scr_floor floorB;
    [SerializeField] private Text textoA;
    [SerializeField] private Text textoB;

    // rangos
    private float minX = -8.5f;
    private float maxX = 8.5f;
    private float minZ = -18.5f;
    private float maxZ = 18.5f;
    private float maxAZ = -1.5f;
    private float minBZ = 1.5f;

    private void Start() {
        InvokeRepeating("SpawnTrash",2,2);
    }

    private void SpawnTrash()
    {
        // 4 valores: 25% de spawn en A, 25% de spawn en B, 50% de no-spawn.
        int val = Random.Range(0,4);
        switch (val)
        {
            case 1:
                SpawnTrashA();
                Debug.Log("Spawn en A");
                break;
            case 2:
                SpawnTrashB();
                Debug.Log("Spawn en B");
                break;
            default:
                Debug.Log("No spawn");
                break;
        }
    }

    private void SpawnTrashA()
    {
        Vector3 randomPos = new Vector3(Random.Range(minX, maxX), 0.5f, Random.Range(minZ, maxAZ));
        Instantiate(prefabs[0],randomPos,Quaternion.identity);
        floorA.HayBasura();
        textoA.text = "Piso A: Sucio";
    }

    private void SpawnTrashB()
    {
        Vector3 randomPos = new Vector3(Random.Range(minX, maxX), 0.5f, Random.Range(minBZ, maxZ));
        Instantiate(prefabs[1],randomPos,Quaternion.identity);
        floorB.HayBasura();
        textoB.text = "Piso B: Sucio";
    }
}
