﻿using UnityEngine;
using UnityEngine.UI;

public class scr_movePlayer : MonoBehaviour
{
    [SerializeField] private Vector3 posA;
    [SerializeField] private Vector3 posB;
    [SerializeField] private float tTotal;
    [SerializeField] private float totalLimpieza;
    [SerializeField] private scr_floor floorA;
    [SerializeField] private scr_floor floorB;
    [SerializeField] private Text textoA;
    [SerializeField] private Text textoB;
    [SerializeField] private Text Mensaje;

    private float tInicial, tLimpieza;
    private bool estoyEnA = true; // true o false
    private bool limpiar = false;
    private bool showMoveText = false;

    private void Start() {
        tInicial = Time.time;
    }

    void Update()
    {
        if (estoyEnA)
        {
            if (!limpiar)
            {
                float fraccion = (Time.time - tInicial) / tTotal;
                transform.position = Vector3.Lerp(posA, posB, fraccion);
                if (showMoveText)
                    Mensaje.text = "Moviendome a B..";
            }            
        }
        else
        {
            if (!limpiar)
            {
                float fraccion = (Time.time - tInicial) / tTotal;
                transform.position = Vector3.Lerp(posB, posA, fraccion);
                if (showMoveText)
                    Mensaje.text = "Moviendome a A..";
            }
        }

        if (Time.time - tInicial > tTotal)
        {
            if (estoyEnA & !limpiar)
            {
                if (floorB.EstaLimpio())
                {
                    Mensaje.text = "B está limpio. Moviendome a A..";
                    showMoveText = false;
                    estoyEnA = !estoyEnA;
                    tInicial = Time.time;
                }
                else
                {
                    limpiar = true;
                    tLimpieza = Time.time;

                    Mensaje.text = "B está sucio. Limpiando..";
                    LimpiarB(); // van cruzados porque todavía no cambie de lado
                }
                
            }
            else if (!estoyEnA & !limpiar)
            {
                if (floorA.EstaLimpio())
                {
                    Mensaje.text = "A está limpio. Moviendome a B..";
                    showMoveText = false;
                    estoyEnA = !estoyEnA;
                    tInicial = Time.time;
                }
                else
                {
                    limpiar = true;
                    tLimpieza = Time.time;

                    Mensaje.text = "A está sucio. Limpiando..";
                    LimpiarA();
                }
            }
            else//hay que limpiar
            {
                if (Time.time - tLimpieza > totalLimpieza)
                {
                    limpiar = !limpiar;
                    showMoveText = true;
                    estoyEnA = !estoyEnA;
                    tInicial = Time.time;
                }
                else
                {
                    if (estoyEnA)
                    {
                        LimpiarB();
                    }    
                    else
                    {
                        LimpiarA();
                    }
                }
            }
        }
    }

    private void LimpiarA()
    {
        GameObject[] trashes;
        trashes = GameObject.FindGameObjectsWithTag("trashA");
        
        foreach(GameObject trash in trashes)
        {
            Destroy(trash);
        }

        floorA.Limpiar();
        textoA.text = "Piso A: Limpio";
    }

    private void LimpiarB()
    {
        GameObject[] trashes;
        trashes = GameObject.FindGameObjectsWithTag("trashB");
        
        foreach(GameObject trash in trashes)
        {
            Destroy(trash);
        }

        floorB.Limpiar();
        textoB.text = "Piso B: Limpio";
    }
}
